import React from 'react';
import { StatusBar } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { Root, StyleProvider } from 'native-base';

import getTheme from "../../native-base-theme/components";
import material from "../../native-base-theme/variables/material";

import TermosDeUso from '../pages/TermosDeUso';

const AppNavigator = createAppContainer(
  createSwitchNavigator({
    TermosDeUso
  }, {
    initialRouteName: 'TermosDeUso',
    headerMode: 'none',
  }),
);

export default () =>
  <Root>
    <StatusBar
      backgroundColor="#0D1B40"
      barStyle="light-content"
    />
    <StyleProvider style={getTheme(material)}>
      <AppNavigator />
    </StyleProvider>
  </Root>;
